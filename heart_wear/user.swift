//
//  user.swift
//  heart_wear
//
//  Created by BS-236 on 28/7/21.
//

import CoreData

@objc (User)
class User: NSManagedObject {
    @NSManaged var firstName: NSString!
    @NSManaged var lastName: NSString!
    @NSManaged var dob: NSString!
    @NSManaged var sex: NSString!
    @NSManaged var phoneNumber: NSString!
    @NSManaged var email: NSString!
    @NSManaged var password: NSString!
}
