//
//  ViewController.swift
//  heart_wear
//
//  Created by BS-236 on 2/7/21.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var field_email: UITextField!
    
    @IBOutlet weak var field_password: UITextField!
    
    @IBOutlet weak var view_emailAddress: UIView!
    
    @IBOutlet weak var view_password: UIView!
    
    
    @IBAction func button_fingerprint(_ sender: Any) {
    }
        
    @IBAction func button_faceID(_ sender: Any) {
    }
    
    @IBOutlet weak var view_login: UIView!
    
    
    @IBOutlet weak var view_createUser: UIView!
    
    @IBOutlet weak var view_forgottenPass: UIView!
    
    
    @IBOutlet weak var switchOutlet_emailPhoneToggle: UISwitch!
    
    
    
    @IBAction func switch_emailPhoneToggle(_ sender: UISwitch) {
                    
            self.field_email.placeholder = "Enter phone number"
            
            if sender.isOn {
                self.field_email.placeholder = "Enter phone number"
            }
            else {
                
                self.field_email.placeholder = "Enter email address"
                
            }
        }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view_emailAddress.layer.cornerRadius = 6;
        view_emailAddress.layer.masksToBounds = true;
        view_emailAddress.layer.borderWidth = 2
        view_emailAddress.layer.borderColor = UIColor.systemGray4.cgColor
        
        switchOutlet_emailPhoneToggle.transform = CGAffineTransform(scaleX: 0.50, y: 0.50)
        
        view_password.layer.cornerRadius = 6;
        view_password.layer.masksToBounds = true;
        view_password.layer.borderWidth = 2
        view_password.layer.borderColor = UIColor.systemGray4.cgColor
        
        view_login.layer.cornerRadius = 6;
        view_login.layer.masksToBounds = true;
        view_login.layer.borderWidth = 2
        view_login.layer.borderColor = UIColor.white.cgColor
        
        view_createUser.layer.cornerRadius = 6;
        view_createUser.layer.masksToBounds = true;
        view_createUser.layer.borderWidth = 2
        view_createUser.layer.borderColor = UIColor.white.cgColor
        
        view_forgottenPass.layer.cornerRadius = 6;
        view_forgottenPass.layer.masksToBounds = true;
        view_forgottenPass.layer.borderWidth = 2
        view_forgottenPass.layer.borderColor = UIColor.white.cgColor
    }


}

@IBDesignable extension UITextField {

    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }

    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
        }
        get {
            return layer.cornerRadius
        }
    }

    @IBInspectable var borderColor: UIColor? {
        set {
            guard let uiColor = newValue else { return }
            layer.borderColor = uiColor.cgColor
        }
        get {
            guard let color = layer.borderColor else { return nil }
            return UIColor(cgColor: color)
        }
    }
}

