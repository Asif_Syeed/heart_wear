//
//  CreateUserViewController.swift
//  heart_wear
//
//  Created by BS-236 on 14/7/21.
//

import UIKit
import CoreData

class CreateUserViewController: UIViewController {
    
    
    @IBAction func button_back(_ sender: Any) {
        if let navController = self.navigationController {
            navController.popViewController(animated: true)
        }
    }
    
    @IBOutlet weak var view_firstName: UIView!
    @IBOutlet weak var view_lastName: UIView!
    @IBOutlet weak var view_dob: UIView!
    @IBOutlet weak var view_sex: UIView!
    
    @IBOutlet weak var field_firstName: UITextField!
    @IBOutlet weak var field_lastName: UITextField!
    @IBOutlet weak var field_phoneNumber: UITextField!
    @IBOutlet weak var field_email: UITextField!
    @IBOutlet weak var field_password: UITextField!
    @IBOutlet weak var field_repassword: UITextField!
    
    
    @IBAction func action_firstName(_ sender: Any) {
        
    }
    
    @IBAction func action_lastName(_ sender: Any) {
        
    }
    
    @IBAction func action_dob(_ sender: Any) {
        
    }
    
    @IBAction func action_sex(_ sender: Any) {
    }
    
    @IBAction func action_phoneNumber(_ sender: Any) {
       
    }
    
    @IBAction func action_email(_ sender: Any) {
       
    }
    
    
    @IBAction func action_password(_ sender: Any) {
        
    }
    
    
    @IBAction func action_repassword(_ sender: Any) {
        
    }
    
    
    
    
    @IBOutlet weak var field_dob: UITextField!
    
    @IBOutlet weak var field_sex: UITextField!
    
    let sex = ["Male", "Female", "Other"]
    var sexPickerView = UIPickerView()
    
    
    @IBOutlet weak var view_phoneNumber: UIView!
    
    @IBOutlet weak var view_email: UIView!
    
    @IBOutlet weak var view_password: UIView!
    
    @IBOutlet weak var view_repassword: UIView!
    
    @IBOutlet weak var view_checkbox: UIView!
    
    
    @IBOutlet weak var view_createUser: UIView!
    
    @IBAction func button_createUser(_ sender: Any) {
        if field_firstName.text != "" && field_lastName.text != "" && field_dob.text != "" && field_sex.text != "" && field_phoneNumber.text != "" && field_email.text != "" && field_password.text != "" && field_repassword.text != "" {
            
            //saving data into model
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context: NSManagedObjectContext = appDelegate.persistentContainer.viewContext
            let entity = NSEntityDescription.entity(forEntityName: "User", in: context)
            let newUser = User(entity: entity!, insertInto: context)
            
            newUser.firstName = field_firstName.text as NSString?
            newUser.lastName = field_lastName.text as NSString?
            newUser.dob = field_dob.text as NSString?
            newUser.sex = field_sex.text as NSString?
            newUser.phoneNumber = field_phoneNumber.text as NSString?
            newUser.email = field_email.text as NSString?
            newUser.password = field_password.text as NSString?
            
            do {
                try context.save()
                
                let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "WelcomeView") as! WelcomeViewController
                self.navigationController?.pushViewController(secondViewController, animated: true)
                
            } catch  {
                print("context save error")
            }
            
            
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view_firstName.layer.cornerRadius = 6;
        view_firstName.layer.masksToBounds = true;
        view_firstName.layer.borderWidth = 2
        view_firstName.layer.borderColor = UIColor.systemGray4.cgColor
        
        view_lastName.layer.cornerRadius = 6;
        view_lastName.layer.masksToBounds = true;
        view_lastName.layer.borderWidth = 2
        view_lastName.layer.borderColor = UIColor.systemGray4.cgColor
        
        view_dob.layer.cornerRadius = 6;
        view_dob.layer.masksToBounds = true;
        view_dob.layer.borderWidth = 2
        view_dob.layer.borderColor = UIColor.systemGray4.cgColor
        
        view_sex.layer.cornerRadius = 6;
        view_sex.layer.masksToBounds = true;
        view_sex.layer.borderWidth = 2
        view_sex.layer.borderColor = UIColor.systemGray4.cgColor
        
        view_phoneNumber.layer.cornerRadius = 6;
        view_phoneNumber.layer.masksToBounds = true;
        view_phoneNumber.layer.borderWidth = 2
        view_phoneNumber.layer.borderColor = UIColor.systemGray4.cgColor

        view_email.layer.cornerRadius = 6;
        view_email.layer.masksToBounds = true;
        view_email.layer.borderWidth = 2
        view_email.layer.borderColor = UIColor.systemGray4.cgColor
        
        view_password.layer.cornerRadius = 6;
        view_password.layer.masksToBounds = true;
        view_password.layer.borderWidth = 2
        view_password.layer.borderColor = UIColor.systemGray4.cgColor
        
        view_repassword.layer.cornerRadius = 6;
        view_repassword.layer.masksToBounds = true;
        view_repassword.layer.borderWidth = 2
        view_repassword.layer.borderColor = UIColor.systemGray4.cgColor
        
        view_checkbox.layer.cornerRadius = 6;
        view_checkbox.layer.masksToBounds = true;
        view_checkbox.layer.borderWidth = 2
        view_checkbox.layer.borderColor = UIColor.systemGray4.cgColor
        
        view_checkbox.layer.cornerRadius = 6;
        view_checkbox.layer.masksToBounds = true;
        view_checkbox.layer.borderWidth = 2
        view_checkbox.layer.borderColor = UIColor.systemGray4.cgColor
        
        view_createUser.layer.cornerRadius = 6;
        view_createUser.layer.masksToBounds = true;
        view_createUser.layer.borderWidth = 2
        view_createUser.layer.borderColor = UIColor.white.cgColor
        
        
        //date picker
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        field_dob.text = formatter.string(from: date)
        field_dob.textColor = .black
        
        let datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(datePickerValueChanged(sender:)), for: UIControl.Event.valueChanged)
        datePicker.frame.size = CGSize(width: 0, height: 120)
        field_dob.inputView = datePicker
        
        
        //sex picker
        field_sex.inputView = sexPickerView
        sexPickerView.delegate = self
        sexPickerView.dataSource = self
        
    }
    
    @objc func datePickerValueChanged (sender: UIDatePicker) {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        field_dob.text = formatter.string(from: sender.date)
    }
    
}

extension CreateUserViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return sex.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return sex[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        field_sex.text = sex[row]
        field_sex.resignFirstResponder()
    }
}


