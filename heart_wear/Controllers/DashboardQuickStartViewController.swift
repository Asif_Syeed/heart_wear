//
//  DashboardQuickStartViewController.swift
//  heart_wear
//
//  Created by BS-236 on 16/7/21.
//

import UIKit

class DashboardQuickStartViewController: UIViewController {
    
    
    @IBOutlet weak var view_warning: UIView!
    
    @IBOutlet weak var view_warningLabel: UIView!
    
    @IBOutlet weak var buttonOutlet_setup: UIButton!
    
    @IBAction func button_setup(_ sender: Any) {
    }
    
    @IBOutlet weak var buttonOutlet_instruction: UIButton!
    
    
    @IBAction func button_instruction(_ sender: Any) {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view_warning.layer.cornerRadius = 10;
        view_warning.layer.masksToBounds = true;
        
        view_warning.layer.shadowOffset = CGSize(width: 3, height: 6)
        view_warning.layer.shadowRadius = 10
        view_warning.layer.shadowOpacity = 1
        
        view_warningLabel.layer.cornerRadius = 10;
        view_warningLabel.layer.masksToBounds = true;
        
        view_warningLabel.layer.borderWidth = 2
        view_warningLabel.layer.borderColor = UIColor.orange.cgColor
        
        
    }
    


}
